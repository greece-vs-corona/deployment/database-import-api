import requests
import os
import logging
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func
from sqlalchemy.ext.automap import automap_base
import csv




def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in { 'csv'}


def deduce_medical_history(med_hist):
    if med_hist == "0.00":
        return("NO")
    if med_hist == "1.00":
        return("CVD")
    if med_hist == "2.00":
        return("AH")
    if med_hist == "3.00":
        return("DM")
    if med_hist == "4.00":
        return("COPD")
    if med_hist == "5.00":
        return("CANCER")
    if med_hist == "6.00":
        return("AUTOIMMUNE")


def deduce_value(val):
    if val == "0.00" or val == "#NULL!":
        return None
    return val


# returns a dictionary with user's information
# currently, we include "id", "username","name" and "email" of a user 
def get_user_information(access_token):
    headers = {"Authorization":"Bearer " + str(access_token)}
    response = requests.get(os.environ.get('GITLAB_USERINFO_URL'),headers=headers).json()
    user_information = {
                            "id": response["id"],
                            "username":response["username"],
                            "name":response["name"],
                            "email":response["email"]
                        }
    return user_information


#returns a list which contains the groups of a user
def get_groups(access_token):
    
    headers = {"Authorization":"Bearer " + str(access_token) }
    response = requests.get(os.environ.get('GITLAB_GROUPS_URL'),headers=headers).json()
    
    groups = []
    for group in response:
        groups.append(group["name"])

    return groups

# Function that returns common elements of lists "list1" and "list2"
def list_intesrection(list1,list2):
    return list(set(list1) & set(list2))


def write_to_db(app,filename,submitter_information):

    AVAILABLE_DRUGS = ["Statin", "StatinDose","ACEi", "Sartan","CaBlocker", "Diuretic","Betablocker","Biguanides" ,"Sulfonylureas" ,"TZDs" ,"GLP1_RAs" ,"DPP4i" , "SGLT2i" ,"Insulin_long_acting" ,"Insulin_long_acting_dose" ,"Insulin_short_acting" ,"Insulin_short_acting_dose" ,"PPI" ,"Corticosteroids" ,"other_immunomodulators" ,"AntiXa","Coumarin"]
    DAYS_OF_INTEREST = [1,3,5,7,9,11,15,28,90]

    EXTRA_DRUGS = ["heparin","remdesivir","tocilizumab","corticosteroids","anakinra","antimalarials","clarithromycin","cotrimoxazole","doxycyclin","penicillins","cefalosporins","flurocinolones","glycopeptides"]
    EXTRA_TIMELINE_CHARACTERISTICS = ["days_to_intubation","days_to_oxygen_supplementation","days_to_clinical_deterioration","day_of_PE_diagnosis","days_of_hospitalization","no.ofdays_patient_recieved_macrolides"]
    EXTRA_CLINICAL_CHARACTERISTICS = ["PE_on_CTPA","CT1_burden_of_disease","CT2_burden_of_disease","HCV_Positive","HBV_Positive","Intubation","Patient_death"]
    PATIENT_STATUS_CHARACTERISTICS = ["patient_status_at_28days","patient_status_at_90days"]



    db = SQLAlchemy(app)
    Base = automap_base()
    Base.prepare(db.engine,reflect=True)

    with open(filename, newline='') as csvfile:
        file_data=csv.reader(csvfile)
        headers=next(file_data)
        my_dict = [dict(zip(headers,i)) for i in file_data]

    Submitter = Base.classes.submitter
    new_submitter = Submitter(  
                                    user_id=submitter_information["id"],
                                    username=submitter_information["username"],
                                    name=submitter_information["name"],
                                    email=submitter_information["email"],
                                    groups=submitter_information["groups"]
                                )
    user_id = db.session.query(Submitter.user_id).filter_by(user_id = submitter_information["id"]).scalar()

    if user_id == None:
        db.session.add(new_submitter)

    for item in my_dict:

        patient_sex = "M" if item["sex"] == "1.00" else "F"
        age = int(float(item["Age"]))
        phone_number = item["Tel_No"]
        Patient = Base.classes.patient

        new_patient = Patient(patient_sex=patient_sex,age=age,phone_number=phone_number)
        db.session.add(new_patient)
        db.session.flush()
        patient_id = new_patient.patient_id 


        patient_admission_date = item["Admission_date"]
        patient_discharge_date =  item["Discharge_date"]
        symptom_duration = int(float(item["duration_of_symptoms"]))
        medical_history = deduce_medical_history(item["Medicalhistory"])
        Disease_Instance = Base.classes.disease_instance

        new_disease_instance = Disease_Instance(patient_id=patient_id,
                                                submitter_id=submitter_information["id"],
                                                patient_admission_date=patient_admission_date,
                                                patient_discharge_date=patient_discharge_date,
                                                medical_history = medical_history,
                                                symptom_duration = symptom_duration)
        db.session.add(new_disease_instance)
        db.session.flush()
        disease_instance_id = new_disease_instance.disease_instance_id 


        Disease_Timeline = Base.classes.disease_timeline
        for day in DAYS_OF_INTEREST:
            new_disease_timeline = Disease_Timeline(disease_instance_id=disease_instance_id,day=day)
            db.session.add(new_disease_timeline)

        for key in item:
            if(key.startswith("risk_factor")):
                risk_factor = deduce_value(item[key])
                if risk_factor:
                    Risk_Factor = Base.classes.risk_factors
                    new_risk_factor = Risk_Factor(disease_instance_id=disease_instance_id,risk_factor_name=key)
                    db.session.add(new_risk_factor)
            elif(key.startswith("symptom")):
                symptom = deduce_value(item[key])
                if symptom:
                    Symptom = Base.classes.symptoms
                    new_symptom = Symptom(disease_instance_id=disease_instance_id,symptom_name=key)
                    db.session.add(new_symptom)

            elif key == "patient_status_at_15days":
                status = 1.0 if item[key] == "#NULL!" else item[key]
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=15)
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=status)
                db.session.add(new_clinical_characteristics_timeline)

            elif key == "patient_status_at_28days":
                status = 1.0 if item[key] == "#NULL!" else item[key]
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=28)
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=status)
                db.session.add(new_clinical_characteristics_timeline)
            elif key == "patient_status_at_90days":
                status = 1.0 if item[key] == "#NULL!" else item[key]
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=90)
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=status)
                db.session.add(new_clinical_characteristics_timeline)

            elif "day15" in key:
                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=15)
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)
            elif "day11" in key:
                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=11)
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)


            elif "day1" in key:
                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=1)
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)


            elif "day3" in key:
                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=3)
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)


            elif "day5" in key:
                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=5)
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)


            elif "day7" in key:
                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=7)
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)


            elif "day9" in key:
                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=9)
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)

            elif key in EXTRA_TIMELINE_CHARACTERISTICS:
                day = 0 if item[key] == "#NULL!" else (int)(float(item[key]))
                if day in DAYS_OF_INTEREST:
                    Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                    disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=day)
                    new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=day)
                    db.session.add(new_clinical_characteristics_timeline)
                elif day not in DAYS_OF_INTEREST:

                    new_disease_timeline = Disease_Timeline(disease_instance_id=disease_instance_id,day=0)
                    row = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id=disease_instance_id,day=0).first()
                    if not row:
                        db.session.add(new_disease_timeline)
                        db.session.flush()
                        disease_timeline_id = new_disease_timeline.disease_timeline_id 
                    else:
                        disease_timeline_id = row.disease_timeline_id

                    Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                    new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=day)
                    db.session.add(new_clinical_characteristics_timeline)
            elif key in EXTRA_CLINICAL_CHARACTERISTICS:
                clinical_characteristic_value = 0.00 if item[key] == "#NULL!" else item[key]
                new_disease_timeline = Disease_Timeline(disease_instance_id=disease_instance_id,day=0)
                row = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id=disease_instance_id,day=0).first()
                if not row:
                    db.session.add(new_disease_timeline)
                    db.session.flush()
                    disease_timeline_id = new_disease_timeline.disease_timeline_id 
                else:
                    disease_timeline_id = row.disease_timeline_id

                Clinical_Characteristics_Timeline = Base.classes.clinical_characteristics_timeline
                new_clinical_characteristics_timeline = Clinical_Characteristics_Timeline(disease_timeline_id=disease_timeline_id,clinical_characteristic_observed=key,clinical_characteristic_value=clinical_characteristic_value)
                db.session.add(new_clinical_characteristics_timeline)


        for key in AVAILABLE_DRUGS:
            drug = deduce_value(item[key])
            if drug:
                Drug = Base.classes.drugs
                new_drug = Drug(disease_instance_id=disease_instance_id,drug_name=key,drug_value=drug)
                db.session.add(new_drug)

        for drug in EXTRA_DRUGS:
            if "patient_received_"+drug in item and deduce_value(item["patient_received_"+drug]):
                Drug_Received_Timeline = Base.classes.drug_received_timeline
                disease_timeline_id = None
                if "day_patient_received_"+drug in item:


                    day = (int)(float(item[key]))

                    if day in DAYS_OF_INTEREST and day != 0:
                        disease_timeline_id = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id = disease_instance_id,day=day).first()
                    elif day not in DAYS_OF_INTEREST and day != 0:
                        new_disease_timeline = Disease_Timeline(disease_instance_id=disease_instance_id,day=day)
                        db.session.add(new_disease_timeline)
                        db.session.flush()
                        disease_timeline_id = new_disease_timeline.disease_timeline_id 
                    elif day == 0:
                        new_disease_timeline = Disease_Timeline(disease_instance_id=disease_instance_id,day=0)
                        row = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id=disease_instance_id,day=0).first()
                        if not row:
                            db.session.add(new_disease_timeline)
                            db.session.flush()
                            disease_timeline_id = new_disease_timeline.disease_timeline_id 
                            
                        else:
                            disease_timeline_id = row.disease_timeline_id


                else:
                    new_disease_timeline = Disease_Timeline(disease_instance_id=disease_instance_id,day=0)
                    row = db.session.query(Disease_Timeline.disease_timeline_id).filter_by(disease_instance_id=disease_instance_id,day=0).first()
                    if not row:
                        db.session.add(new_disease_timeline)
                        db.session.flush()
                        disease_timeline_id = new_disease_timeline.disease_timeline_id 

            
                    else:
                        disease_timeline_id = row.disease_timeline_id

                if drug + "_dosage" in item:
                    drug_dosage = 0.0 if item[drug + "_dosage"] == "#NULL!" else item[drug + "_dosage"]
                    new_drug_received_timeline = Drug_Received_Timeline(disease_timeline_id=disease_timeline_id,drug_received=drug,drug_dosage=drug_dosage)
                    db.session.add(new_drug_received_timeline)
                else:

                    new_drug_received_timeline = Drug_Received_Timeline(disease_timeline_id=disease_timeline_id,drug_received=drug,drug_dosage=0.00)
                    db.session.add(new_drug_received_timeline)

    db.session.commit()
    db.session.close()
    return ("")
