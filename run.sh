#!/bin/sh

gunicorn -b 0.0.0.0:8000 app:app --workers=$GUNICORN_WORKERS --threads=$GUNICORN_THREADS --timeout 180 --log-level debug
