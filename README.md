
# REST API for importing Clinical Data Service datasets

## Authentication
Requests to the REST API need to be authenticated with an authentication token. Platform users can aquire the token with the following command:


`ACCESS_TOKEN=$(curl -d 'client_id=<CLIENT_ID>' -d 'client_secret=<CLIENT_SECRET>' -d 'username=<YOUR_USERNAME>' -d 'password=<YOUR_PASSWORD>' -d 'grant_type=password' -X POST "https://gitlab.repository.greecevscorona.gr/oauth/token/" | grep -Po '"access_token":.*?[^\\]"' | cut -d ':' -f2 | tr -d '"')`

`<CLIENT_ID>` and `<CLIENT_SECRET>` must be provided by the authentication service administrator.


## Data import
To import a dataset in CSV file format issue a POST request at `/upload_csv`:

`curl -X POST -H "Authorization:Bearer $ACCESS_TOKEN" -F file=@<CLINICAL_DATA_CSV> "https://rest.staging.clinical-data-service.greecevscorona.gr/upload_csv"`

Where `<CLINICAL_DATA_CSV>` the path to the CSV file.

If submission is successful, `{status:Uploaded}` will be returned.

## Data Visualization
You can browse through data that are stored in the databasse at: https://metabase.staging.clinical-data-service.greecevscorona.gr/

In order to get access,  you will need to request  `<METABASE_USERNAME>` and `<METABASE_PASSWORD>` from the authentication service administrator.

After successful login, you can follow: https://metabase.staging.clinical-data-service.greecevscorona.gr/browse/33-covid-19-demokritos to see all available tables in database.

You will be redirected to a page looking like:
![alt tag](https://i.imgur.com/hBwQCbD.png)



By clicking on any table, you will be able to see data that are stored.
If you want to query a value, you can click `"Filter"` button in the top right corner.

If you want to apply some other function in the table (such as count or any other metric), you can click `"Summarize"` button in the top right corner. 	

In the left bottom corner, there is a `"Visualization"` option which gives you multiple options about the way you want to demonstrate your data.

For example, if you want to inspect "Disease Instance" table, you will get the following page:
![alt tag](https://i.imgur.com/UGjhh6h.png)

By clicking in the Visualization option, you can specify the way that your data will be visualized.
For example, you can create a Bar Chart that demonstrates the duration of symptoms for every patient. 

The produced chart will look like this:
![alt tag](https://i.imgur.com/VUJ9SKa.png)






