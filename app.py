import ast
import logging
import os
import uuid

from flask import Flask, flash, request, redirect
from flask_oidc import OpenIDConnect
from werkzeug.utils import secure_filename

from routines.helpers import get_groups, get_user_information, write_to_db, list_intesrection, allowed_file

logging.basicConfig(level=logging.DEBUG)

POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
POSTGRES_SERVER = os.environ.get('POSTGRES_SERVER')
POSTGRES_DB = os.environ.get('POSTGRES_DB')
AVAILABLE_GROUPS = ast.literal_eval(os.environ.get('GROUPS'))

app = Flask(__name__)

app.config.update({
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'OIDC_CLIENT_SECRETS': '/etc/oidc_client_secrets/client_secrets.json',
    'SECRET_KEY': os.environ.get('FLASK_SECRET_KEY'),
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
    'SQLALCHEMY_DATABASE_URI': "postgresql://" + POSTGRES_USER + ":" + POSTGRES_PASSWORD + "@" + POSTGRES_SERVER + "/" + POSTGRES_DB
})

oidc = OpenIDConnect(app)

CSV_UPLOAD_FOLDER = './uploaded_csv'
if not os.path.exists(CSV_UPLOAD_FOLDER):
    os.makedirs(CSV_UPLOAD_FOLDER)


@app.route('/upload_csv', methods=['POST'])
@oidc.accept_token(require_token=True)
def upload_file():
    if request.method == 'POST':
        access_token = request.headers.get('Authorization').split(' ')[1]
        groups_list = get_groups(access_token)
        submitter_information = get_user_information(access_token)

        # convert list of groups to the corresponding string
        # if groups = ['a','b','c'], converted_groups = " 'a','b','c' "
        groups = ",".join(groups_list)

        submitter_information["groups"] = groups

        if list_intesrection(groups_list, AVAILABLE_GROUPS):
            # check if the post request has the file part
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files['file']
            if file and allowed_file(file.filename):
                filename = str(uuid.uuid4()) + "-" + secure_filename(file.filename)
                file.save(os.path.join(CSV_UPLOAD_FOLDER, filename))
                write_to_db(app, os.path.join(CSV_UPLOAD_FOLDER, filename), submitter_information)
                os.remove(os.path.join(CSV_UPLOAD_FOLDER, filename))

                return {"status": "Uploaded"}
        else:
            return {"status": "Not a member of a group"}


if __name__ == '__main__':
    app.run()
